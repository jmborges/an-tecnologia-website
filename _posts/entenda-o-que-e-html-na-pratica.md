---
title: 'Entenda o que é HTML na prática'
date: '2022-06-26'
excerpt: 'Como uma página web é formada? Como o código se transforma em um site?'
coverImage: '/images/html_code.jpg'
ogImage:
url: '/images/html_code.jpg'
---


_Como uma página web é formada? Como o código se transforma em um site?_

Se você não está dentro do meio da tecnologia da informação, desenvolvimento web e áreas relacionadas, pode parecer um tanto quanto um mistério como se dá o funcionamento de um website, ou de um navegador web.

Se você tem curiosidade a respeito, queremos explicar algumas coisas que podem ajudar a entender melhor.

Antes de explicarmos o que é o famoso código HTML e como ele funciona, queremos mostrar um exemplo prático para ficar mais fácil de entender o conceito.

### Um exemplo básico de HTML

Para começar, abra um editor de texto qualquer, como o bloco de notas do computador, por exemplo.

![Captura de tela de um editor de texto em branco](/images/post-1-img-1.jpg)

Agora, você pode digitar ou copiar e colar o conteúdo abaixo no editor de texto:
```
<h1>
    Olá mundo!
</h1>
<h2>
    Bem-vindo ao meu site
</h2>
<input>
<button>
    Clique aqui
</button>
```

![Captura de tela de um editor de texto com código HTML escrito](/images/post-1-img-2.jpg)

Essas palavras envolvidas nos símbolos “maior que” e “menor que” (< >) são chamadas de tags HTML.

Depois, salve o arquivo em um lugar que você consiga encontrar facilmente com o nome “teste.html”. 

![Captura de tela do processo de salvamento de arquivo em um editor de texto](/images/post-1-img-3.jpg)

Não se esqueça de verificar se você está salvando o arquivo com a extensão .html, ao invés de .txt, como por exemplo “teste.html.txt”. Isso pode ocorrer caso seu computador esteja configurado para não exibir as extensões dos arquivos.

Caso você tenha salvo o arquivo com a extensão .html de forma correta, você provavelmente vai ver que o ícone do arquivo é o mesmo ícone do navegador web padrão que você tem no computador.

![Captura de tela de um ícone de navegador web](/images/post-1-img-4.jpg)

Agora abra esse arquivo no navegador e veja o que acontece:

![Captura de tela de um navegador web com elementos HTML simples](/images/post-1-img-5.jpg)

### O que aconteceu

O navegador entendeu que esse era um arquivo do tipo HTML, e buscou renderizar (ou “montar”) uma visualização com base no que estava escrito no arquivo.

### A famosa “Hyper-text mark-up language” – o HTML

O que nós chamamos de código HTML é nada mais do que uma “linguagem de marcação”, ou seja, é um conteúdo que possui indicações do que deve ser representado com cada elemento, que no HTML chamamos de “tags”.

O HTML surgiu no início da web, nos anos 90, com uma proposta do físico Tim Berners-Lee, e continua até hoje sendo o padrão básico de construção de páginas web (claro que sofreu uma série de melhorias e adaptações nos últimos 30 anos).

### Como isso funciona

O software que realiza essa função de interpretar e renderizar a página é bastante grande e complexo (o navegador web ou web browser), envolvendo vários arquivos e conhecimentos que vão fugir do escopo desse artigo curto, mas, resumidamente:

Ele verifica cada tag HTML (trecho especial do código envolvido nos símbolos < >) e entende que onde encontrar uma tag do tipo <input> por exemplo, ele deve mostrar um campo de formulário, ou onde encontrar uma tag do tipo <button>, ele deve mostrar um botão, e assim por diante, com todas as tags.

Parece simples demais? De certa forma, o básico do funcionamento é esse mesmo. 
Porém, para que um site tenha um design realmente bonito, seja rápido no carregamento, seja responsivo aos diferentes formatos de tela, é necessária uma quantidade considerável de estudo aprofundado desse tópico e outros que estão relacionados.

Salve o site da AN Tecnologia nos seus favoritos, e nos acompanhe nas redes sociais para continuar aprendendo sobre websites e tecnologia da informação, e como isso tudo pode te ajudar a melhorar seu negócio.
