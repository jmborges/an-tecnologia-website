import nodemailer from "nodemailer";

export default async (req, res) => {
  const { name, last_name, email, message, subject } = req.body;
  const transporter = nodemailer.createTransport({
    host: "smtps.uhserver.com",
    port: 465,
    secure: true,
    auth: {
      user: 'contato@an.tec.br',
      pass: 'planta135'
    },
    tls: { rejectUnauthorized: false }
  });

  try {
    await transporter.sendMail({
      from: "contato@an.tec.br",
      to: "contato@an.tec.br",
      subject: `${subject}`,
      html: `<p>Você tem um envio de formulário de contato</p><br>
        <p><strong>Nome: </strong> ${name} ${last_name}</p><br>
        <p><strong>Email: </strong> ${email}</p><br>
        <p><strong>Messagem: </strong> ${message}</p><br>
      `
    });
  } catch (error) {
    return res.status(500).json({ error: error.message || error.toString() });
  }
  return res.status(200).json({ error: "" });
};