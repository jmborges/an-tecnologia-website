import { useRouter } from 'next/router'
import { getAllPosts } from '../../scripts/blog/getAllPosts'
import { getPostBySlug } from '../../scripts/blog/getAllPosts'
import Image from 'next/image'
import DateFormatter from '../../components/blog/DateFormatter'
import Styles from '../../styles/Blog.module.css'

export default function Post({ post }) {
    return (
        <section>
            <div className='container'>
                <div className='row justify-content-center mt-5 mb-5'>
                    <div className='col-md-8'>
                        <article className={Styles.container_page_post}>
                            <div>
                                <Image src={post.coverImage} height={620}
                                    width={1240} title={post.title} alt={post.title} priority />
                            </div>
                            <h1>{post.title}</h1>

                            <div
                                dangerouslySetInnerHTML={{ __html: post.content }}
                            />
                            <div className={Styles.date}>
                                <DateFormatter dateString={post.date} />
                            </div>
                        </article>
                    </div>
                </div>

            </div>
        </section>
    )
}

export async function getStaticProps({ params }) {
    const post = await getPostBySlug(params.slug, [
        'title',
        'content',
        'coverImage',
        'date',
    ]);

    return {
        props: {
            post: {
                slug: params.slug,
                ...post,
            },
        },
    }
}

export async function getStaticPaths() {
    const posts = await getAllPosts(['slug'])
    const paths = posts.map((post) => {
        return {
            params: {
                slug: post.slug,
            },
        }
    })

    return {
        paths,
        fallback: false,
    }
}

