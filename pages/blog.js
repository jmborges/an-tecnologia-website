import Link from "next/link"
import { getAllPosts } from "../scripts/blog/getAllPosts"
import Styles from "../styles/Blog.module.css"
import Post from "../components/blog/Post"
export default function Blog(props) {
    const { allPosts } = props;
    return (
        <section className={Styles.section_blog}>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <h1 className={Styles.title_page}>Blog</h1>
                    </div>
                </div>
                <div className="row mt-5 p-md-5">
                    {allPosts.map((post) => (
                        <Post title={post.title}
                            slug={post.slug}
                            coverImage={post.coverImage}
                            excerpt={post.excerpt}
                            date={post.date}
                            key={post.title}
                        />
                    ))}
                </div>
            </div>
        </section >
    )
}
export async function getStaticProps() {
    const allPosts = await getAllPosts([
        'title',
        'slug',
        'excerpt',
        'coverImage',
        'date'
      ])
    return {
        props: {
            allPosts
        }
    }
}