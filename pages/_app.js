import '../styles/globals.css'
import CookieConsent, { Cookies } from "react-cookie-consent";
import Link from 'next/link';
import Layout from '../components/Layout'
function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps} />
      <CookieConsent
        location="bottom"
        buttonText="Aceito"
        cookieName="AcceptCookieSite"
        style={{ background: "#174580", color: "#FFFFFF", display: "flex", justifyContent: "start" }}
        buttonStyle={{
          background: "#FFFFFF",
          color: "#174580",
          border: "1px solid #174580",
          fontSize: "20px",
          fontWeight: "700",
          padding: "5px 25px",
          borderRadius: "5px",
          textDecoration: "none",
          opacity: "1",
          transition: "all .2s ease",
        }}
        expires={150}
      >
        <p style={{ fontWeight: "700" }}>Este website utiliza cookies próprios e de terceiros para melhoria da experiência do usuário. Utilizá-lo significa aceitar nossa <Link href="/privacyPolicy"><a style={{color: "#FFFFFF", textAlign: "center", }}>Política de Privacidade</a></Link>.</p>
      </CookieConsent>
    </Layout>
  )
}

export default MyApp
