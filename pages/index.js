import ProjectRow from '../components/index/ProjectRow'
import ServicesRow from '../components/index/ServicesRow'
import BudgetRow from '../components/index/BudgetRow'
import ContactRow from '../components/index/ContactRow'
import Clients from '../components/index/ClientsRow'
import Portifolio from '../components/index/PortifolioRow'
export default function Home() {
  return (
    <>
      <ProjectRow />
      <ServicesRow />
      <Portifolio />
      {/* <Clients/> */}
      <BudgetRow />
      <ContactRow />
    </>
  )
}
