/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
}

module.exports = {
  i18n: {
    locales: ['pt-BR','en-US'],
    defaultLocale: 'pt-BR',
      },
  nextConfig
} 

