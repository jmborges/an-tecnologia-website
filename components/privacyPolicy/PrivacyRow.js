import Link from 'next/link'
import Styles from '../../styles/privacyPolicy.module.css'
export default function privacyRow() {
    return (
        <>
            <section>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-10">
                            <div className={Styles.container_privacy}>
                                <h1>Politica de privicidade</h1>
                                <p>Este site é mantido e operado por AN TECNOLOGIA LTDA. Nós coletamos e utilizamos alguns dados pessoais que pertencem
                                    àqueles que utilizam nosso site. Ao fazê-lo, agimos na qualidade de controlador desses dados e estamos sujeitos às disposições da Lei Federal n. 13.709/2018 (Lei Geral de Proteção de Dados Pessoais - LGPD).
                                    Nós cuidamos da proteção de seus dados pessoais e, por isso, disponibilizamos esta política de privacidade, que contém informações importantes sobre:</p>
                                <ul>
                                    <li>Quem deve utilizar nosso site;</li>
                                    <li>Quais dados coletamos e o que fazemos com eles;</li>
                                    <li>Seus direitos em relação aos seus dados pessoais;</li>
                                    <li>Como entrar em contato conosco.</li>
                                </ul>
                                <p className={Styles.subtitle_privacy}>1. Dados que coletamos e motivos da coleta</p>
                                <p>Nosso site coleta e utiliza alguns dados pessoais de nossos usuários, de acordo com o disposto nesta seção.</p>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>1.1. Dados pessoais fornecidos expressamente pelo usuário</p>
                                    <p>Nós coletamos os seguintes dados pessoais que nossos usuários nos fornecem expressamente ao utilizar nosso site: </p>
                                    <p>- Nome completo;</p>
                                    <p>- Endereço de e-mail;</p>
                                    <p>A coleta destes dados ocorre nos seguintes momentos:</p>
                                    <p>- Quando o usuário preenche o formulário de contato presente no site.</p>
                                    <p>Os dados fornecidos por nossos usuários são coletados com as seguintes finalidades:</p>
                                    <p>- Entrar em contato com o usuário;</p>
                                </div>

                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>1.2. Dados pessoais obtidos de outras formas</p>
                                    <p>Nós coletamos os seguintes dados pessoais de nossos usuários:</p>
                                    <p>- Endereço IP;</p>
                                    <p>- Dados de geolocalização;</p>
                                    <p>- Informações do navegador e sistema operacional utilizado;</p>
                                    <p>A coleta destes dados ocorre nos seguintes momentos:</p>
                                    <p>- Quando o usuário carrega qualquer página do site;</p>
                                    <p>- Quando o usuário preenche o formulário de contato presente no site.</p>
                                    <p>Estes dados são coletados com as seguintes finalidades:</p>
                                    <p>- Garantir a segurança e a autenticidade das transações feitas no site;</p>
                                    <p>- Cumprir determinação legal de armazenamento de registros de acessos a aplicações;</p>
                                    <p>- Personalizar e melhorar a experiência do usuário</p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>1.3. Dados sensíveis</p>
                                    <p>Não serão coletados dados sensíveis de nossos usuários, assim entendidos aqueles definidos nos arts. 11 e seguintes da Lei de Proteção de Dados Pessoais. Assim, não haverá coleta de dados sobre origem racial ou étnica, convicção religiosa, opinião política, filiação a sindicato ou a organização de caráter religioso, filosófico ou político, dado referente à saúde ou à vida sexual, dado genético ou biométrico, quando vinculado a uma pessoa natural.
                                    </p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>1.4. Cookies</p>
                                    <p>Cookies são pequenos arquivos de texto baixados automaticamente em seu dispositivo quando você acessa e navega por um site. Eles servem, basicamente, para que seja possível identificar dispositivos, atividades e preferências de usuários. Os cookies não permitem que qualquer arquivo ou informação sejam extraídos do disco rígido do usuário, não sendo possível, ainda, que, por meio deles, se tenha acesso a informações pessoais que não tenham partido do usuário ou da forma como utiliza os recursos do site</p>
                                    <p>a. Cookies do site</p>
                                    <p>Os cookies do site são aqueles enviados ao computador ou dispositivo do usuário e administrador exclusivamente pelo site. As informações coletadas por meio destes cookies são utilizadas para melhorar e personalizar a experiência do usuário, sendo que alguns cookies podem, por exemplo, ser utilizados para lembrar as preferências e escolhas do usuário, bem como para o oferecimento de conteúdo personalizado.</p>
                                    <p>b. Cookies de terceiros</p>
                                    <p>Alguns de nossos parceiros podem configurar cookies nos dispositivos dos usuários que acessam nosso site. Estes cookies, em geral, visam possibilitar que nossos parceiros possam oferecer seu conteúdo e seus serviços ao usuário que acessa nosso site de forma personalizada, por meio da obtenção de dados de navegação extraídos a partir de sua interação com o site. O usuário poderá obter mais informações sobre os cookies de terceiro e sobre a forma como os dados obtidos a partir dele são tratados, além de ter acesso à descrição dos cookies utilizados e de suas características, acessando o seguinte link:</p>
                                    <div>
                                        <Link href={'https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage'}><a> https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage</a></Link>
                                    </div>
                                    <div>
                                        <Link href={'https://www.tawk.to/privacy-policy'}><a>https://www.tawk.to/privacy-policy</a></Link>
                                    </div>
                                    <p>As entidades encarregadas da coleta dos cookies poderão ceder as informações obtidas a terceiros.</p>
                                    <p>c. Gestão de cookies</p>
                                    <p>O usuário poderá se opor ao registro de cookies pelo site, bastando que desative esta opção no seu próprio navegador. Mais informações sobre como fazer isso em alguns dos principais navegadores utilizados atualmente podem ser acessadas a partir dos seguintes links:
                                        Internet Explorer: https://support.microsoft.com/pt-br/help/17442/windows-internet-explorer-delete-manage-cookies
                                        Safari: https://support.apple.com/pt-br/guide/safari/sfri11471/mac
                                        Google Chrome: https://support.google.com/chrome/answer/95647?hl=pt-BR&amp;hlrm=pt
                                        Mozila Firefox: https://support.mozilla.org/pt-BR/kb/ative-e-desative-os-cookies-que-os-sites-usam
                                        Opera: https://www.opera.com/help/tutorials/security/privacy/
                                        A desativação dos cookies, no entanto, pode afetar a disponibilidade de algumas ferramentas e funcionalidades do site, comprometendo seu correto e esperado funcionamento. Outra consequência possível é remoção das preferências do usuário que eventualmente tiverem sido salvas, prejudicando sua experiência.
                                    </p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>1.5. Coleta de dados não previstos expressamente</p>
                                    <p>Eventualmente, outros tipos de dados não previstos expressamente nesta Política de Privacidade poderão ser coletados, desde que sejam fornecidos com o consentimento do usuário, ou, ainda, que a coleta seja permitida com fundamento em outra base legal prevista em lei. Em qualquer caso, a coleta de dados e as atividades de tratamento dela decorrentes serão informadas aos usuários do site.</p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>2. Compartilhamento de dados pessoais com terceiros</p>
                                    <p>Nós não compartilhamos seus dados pessoais com terceiros. Apesar disso, é possível que o façamos para cumprir alguma determinação legal ou regulatória, ou, ainda, para cumprir alguma ordem expedida por autoridade pública.</p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>3. Por quanto tempo seus dados pessoais serão armazenados</p>
                                    <p>Os dados pessoais coletados pelo site são armazenados e utilizados por período de tempo que corresponda ao necessário para atingir as finalidades elencadas neste documento e que considere os direitos de seus titulares, os direitos do controlador do site e as disposições legais ou regulatórias aplicáveis. Uma vez expirados os períodos de armazenamento dos dados pessoais, eles são removidos de nossas bases de dados ou anonimizados, salvo nos casos em que houver a possibilidade ou a necessidade de armazenamento em virtude de disposição legal ou regulatória.</p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>4. Bases legais para o tratamento de dados pessoais</p>
                                    <p>Cada operação de tratamento de dados pessoais precisa ter um fundamento jurídico, ou seja, uma base legal, que nada mais é que uma justificativa que a autorize, prevista na Lei Geral de Proteção de Dados Pessoais. Todas as Nossas atividades de tratamento de dados pessoais possuem uma base legal que as fundamenta, dentre as permitidas pela legislação. Mais informações sobre as bases legais que utilizamos para operações de tratamento de dados pessoais específicas podem ser obtidas a partir de nossos canais de contato, informados ao final desta Política.</p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>5. Direitos do usuário</p>
                                    <p>A AN TECNOLOGIA LTDA. se compromete a cumprir integralmente a regulamentação brasileira, em respeito aos princípios abaixo, relativos aos dados pessoais do usuário, que serão:</p>
                                    <ul>
                                        <li>Processados de forma lícita, leal e transparente (licitude, lealdade e transparência);</li>
                                        <li>Coletados apenas para finalidades determinadas, explícitas e legítimas, não podendo ser tratados posteriormente de uma forma incompatível com essas finalidades (limitação das finalidades);</li>
                                        <li>Coletados de forma adequada, pertinente e limitada às necessidades do objetivo para os quais eles são processados (minimização dos dados);</li>
                                        <li>Exatos e atualizados sempre que necessário, de maneira que os dados inexatos sejam apagados ou retificados quando possível (exatidão);</li>
                                        <li>Conservados de uma forma que permita a identificação dos titulares dos dados apenas durante o período necessário para as finalidades para as quais são tratados (limitação da conservação);</li>
                                        <li>Tratados de forma segura, protegidos do tratamento não autorizado ou ilícito e contra a sua perda, destruição ou danificação acidental, adotando as medidas técnicas ou organizativas adequadas (integridade e confidencialidade).</li>
                                    </ul>
                                    <p>O usuário do site possui os seguintes direitos, conferidos pela Lei de Proteção de Dados Pessoais e pelo RGPD:</p>
                                    <ul>
                                        <li>Direito de confirmação e acesso: é o direito do usuário de obter do site a confirmação de que os dados pessoais que lhe digam respeito são ou não objeto de tratamento e, se for esse o caso, o direito de acessar os seus dados pessoais;</li>
                                        <li>Direito de retificação: é o direito do usuário de obter do site, sem demora injustificada, a retificação dos dados pessoais inexatos que lhe digam respeito;</li>
                                        <li>Direito à eliminação dos dados (direito ao esquecimento): é o direito do usuário de ter seus dados apagados do site;</li>
                                        <li>Direito à limitação do tratamento dos dados: é o direito do usuário de limitar o tratamento de seus dados pessoais, podendo obtê-la quando contesta a exatidão dos dados, quando o tratamento for ilícito, quando o site não precisar mais dos dados para as finalidades propostas e quando tiver se oposto ao tratamento dos dados e em caso de tratamento de dados desnecessários;</li>
                                        <li>Direito de oposição: é o direito do usuário de, a qualquer momento, se opor por motivos relacionados com a sua situação particular, ao tratamento dos dados pessoais que lhe digam respeito, podendo se opor ainda ao uso de seus dados pessoais para definição de perfil de marketing (profiling);</li>
                                        <li>Direito de portabilidade dos dados: é o direito do usuário de receber os dados pessoais que lhe digam respeito e que tenha fornecido ao site, num formato estruturado, de uso corrente e de leitura automática, e o direito de transmitir esses dados a outro site;</li>
                                        <li>Direito de não ser submetido a decisões automatizadas: é o direito do usuário de não ficar sujeito a nenhuma decisão tomada exclusivamente com base no tratamento automatizado, incluindo a definição de perfis (profiling), que produza efeitos na sua esfera jurídica ou que o afete significativamente de forma similar.</li>
                                    </ul>
                                    <p>Para exercer seus direitos, o titular poderá enviar um e-mail ou uma correspondência ao nosso Encarregado de Proteção de Dados Pessoais. As informações necessárias para isso estão na seção <span>&quot;</span>Como entrar em contato conosco<span>&quot;</span> desta Política de Privacidade.</p>
                                    <ul>
                                        <li>Nome completo ou razão social, número do CPF (Cadastro de Pessoas Físicas, da Receita Federal do Brasil) ou CNPJ (Cadastro Nacional de Pessoa Jurídica, da Receita Federal do Brasil) e endereço de e-mail do usuário e, se for o caso, do seu representante;</li>
                                        <li>Direito que deseja exercer junto ao site</li>
                                        <li>Data do pedido e assinatura do usuário;</li>
                                        <li>Todo documento que possa demonstrar ou justificar o exercício de seu direito.</li>
                                    </ul>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>6. Medidas de segurança no tratamento de dados pessoais</p>
                                    <p>Empregamos medidas técnicas e organizativas aptas a proteger os dados pessoais de acessos não autorizados e de situações de destruição, perda, extravio ou alteração desses dados. As medidas que utilizamos levam em consideração a natureza dos dados, o contexto e a finalidade do tratamento, os riscos que uma eventual violação geraria para os direitos e liberdades do usuário, e os padrões atualmente empregados no mercado por empresas semelhantes à nossa.</p>
                                    <p>Entre as medidas de segurança adotadas por nós, destacamos as seguintes:</p>
                                    <p>- Restrição de acessos a bancos de dados;</p>
                                    <p>- Certificado SSL</p>
                                    <p>Ainda que adote tudo o que está ao seu alcance para evitar incidentes de segurança, é possível que ocorra algum problema motivado exclusivamente por um terceiro - como em caso de ataques de hackers ou crackers ou, ainda, em caso de culpa exclusiva do usuário, que ocorre, por exemplo, quando ele mesmo transfere seus dados a terceiro. Assim, embora sejamos, em geral, responsáveis pelos dados pessoais que tratamos, nos eximimos de responsabilidade caso ocorra uma situação excepcional como essas, sobre as quais não temos nenhum tipo de controle.
                                        De qualquer forma, caso ocorra qualquer tipo de incidente de segurança que possa gerar risco ou dano relevante para qualquer de nossos usuários, comunicaremos os afetados e a Autoridade Nacional de Proteção de Dados acerca do ocorrido, em conformidade com o disposto na Lei Geral de Proteção de Dados.</p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>7. Reclamação a uma autoridade de controle</p>
                                    <p>Sem prejuízo de qualquer outra via de recurso administrativo ou judicial, todos os titulares de dados têm direito a apresentar reclamação a uma autoridade de controle. A reclamação poderá ser feita à autoridade da sede da AN TECNOLOGIA LTDA., do país de residência habitual do usuário, do seu local de trabalho ou do local onde foi alegadamente praticada a infração.</p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>8. Alterações nesta política</p>
                                    <p>A presente versão desta Política de Privacidade foi atualizada pela última vez em: 14/06/2022</p>
                                    <p>Reservamo-nos o direito de modificar, a qualquer momento, as presentes normas, especialmente para adaptá-las às eventuais alterações feitas em nosso site, seja pela disponibilização de novas funcionalidades, seja pela supressão ou modificação daquelas já existentes. Sempre que houver uma modificação, nossos usuários serão notificados acerca da mudança.</p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>9. Como entrar em contato conosco</p>
                                    <p>Para esclarecer quaisquer dúvidas sobre esta Política de Privacidade ou sobre os dados pessoais que tratamos, entre em contato com nosso Encarregado de Proteção de Dados Pessoais, por algum dos canais mencionados abaixo: </p>
                                    <p>E-mail: contato@an.tec.br</p>
                                    <p>Telefone: +55 (54) 9 9942 4184</p>
                                </div>
                                <div className={Styles.content_privacy}>
                                    <p className={Styles.subtitle_privacy}>10. Do Direito aplicável e do foro</p>
                                    <p>Para a solução das controvérsias decorrentes do presente instrumento, será aplicado integralmente o Direito brasileiro. Os eventuais litígios deverão ser apresentados no foro da comarca de Passo Fundo, Rio Grande do Sul, Brasil</p>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}