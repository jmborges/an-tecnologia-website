import Styles from "../../styles/ProjectRow.module.css"
import Image from "next/image"
import Link from "next/link"

export default function ProjectRow() {
    return (
        <>
            <section className={Styles.bg_slider} id="o_que_pensamos">
                <div className="container">
                    <div className="row justify-content-start">
                        <div className="col-md-9">
                            <h1 className={Styles.title_site}>
                                Sites, Apps, Software personalizado. Somos AN Tecnologia.
                            </h1>
                        </div>
                    </div>
                    <div className="row justify-content-start">
                        <div className="col-md-5">
                            <div className={Styles.bg_site_text}>
                                <p>
                                    Auxiliamos você e sua empresa a ganhar visibilidade, vender mais e melhorar seus processos por meio da tecnologia da informação.
                                </p>
                            </div>

                            <Link href='/#contato'><a><button className={Styles.button_budget + ' btn'}>Quero fazer um orçamento</button></a></Link>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}