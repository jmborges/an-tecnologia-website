import Link from 'next/link'
import Styles from '../../styles/BudgetRow.module.css'
export default function BudgetRow() {
    return (
        <section className={Styles.container_budget} id="orcamento">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6 mb-5">
                        <div className={Styles.content_budget}>
                            <div className={Styles.content_budget_title}>
                                <p>Website profissional</p>
                            </div>
                            <div className="content-budget-text">
                                <div className={Styles.content_budget_price}>
                                    <p><span>À partir de</span> R$ 790,00</p>
                                </div>
                            </div>
                            <div className={Styles.content_budget_packet}>
                                <ul>
                                    <li>Briefing e planejamento da melhor solução para o site</li>
                                    <li>Elaboração de design das páginas (responsivo para desktop, tablet, celular)</li>
                                    <li>Execução do projeto após aprovação</li>
                                    <li>Certificado SSL</li>
                                    <li>Google Analytics e Meta Pixel</li>
                                    <li>Ferramenta de chat em tempo real​</li>
                                    <li>Site em conformidade com a LGPD</li>
                                    <li>Treinamento para uso do site</li>
                                </ul>
                            </div>
                            <div className={Styles.content_budget_packet_text}>
                                <p>Com desconto à vista ou parcelado em até 12x</p>
                                <p>Com até 5 páginas*</p>
                            </div>
                            <div className={Styles.content_budget_button}>
                                <Link href='/#contato'><a><button>Orçamento</button></a></Link>
                            </div>
                        </div>

                    </div>
                    <div className="col-md-6 mb-5">
                        <div className={Styles.content_budget}>
                            <div className={Styles.content_budget_title}>
                                <p>Loja virtual (E-Commerce)</p>
                            </div>
                            <div className="content-budget-text">
                                <div className={Styles.content_budget_price}>
                                    <p><span>À partir de</span> R$ 1.890,00</p>
                                </div>
                            </div>
                            <div className={Styles.content_budget_packet}>
                                <ul>
                                    <li>Briefing e planejamento da melhor solução para o site</li>
                                    <li>Elaboração de design das páginas (responsivo para desktop, tablet, celular)</li>
                                    <li>Página para administração da loja virtual</li>
                                    <li>Pagamento por cartão de crédito e boleto*</li>
                                    <li>Execução do projeto após aprovação</li>
                                    <li>Certificado SSL</li>
                                    <li>Google Analytics e Meta Pixel</li>
                                    <li>Ferramenta de chat em tempo real​</li>
                                    <li>Site em conformidade com a LGPD</li>
                                    <li>Treinamento para uso do site</li>
                                </ul>
                            </div>
                            <div className={Styles.content_budget_packet_text}>
                                <p>Com desconto à vista ou parcelado em até 12x</p>
                                <p>É necessária análise para verificar a melhor solução de pagamentos online*</p>
                            </div>
                            <div className={Styles.content_budget_button}>
                                <Link href='/#contato'><a><button>Orçamento</button></a></Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row justify-content-center'>
                    <div className="col-md-6 mb-5">
                        <div className={Styles.content_budget}>
                            <div className={Styles.content_budget_title}>
                                <p>Aplicativo mobile (App para smartphone)</p>
                            </div>
                            <div className="content-budget-text">
                                <div className={Styles.content_budget_price}>
                                    <p><span>À partir de</span> R$ 2850,00</p>
                                </div>
                            </div>
                            <div className={Styles.content_budget_packet}>
                                <ul>
                                    <li>Briefing e planejamento da melhor solução para o app</li>
                                    <li>Elaboração de design das telas</li>
                                    <li>Execução do projeto após aprovação</li>
                                    <li>App para Android e iOS</li>
                                    <li>Publicação na Play Store e App Store</li>
                                </ul>
                            </div>
                            <div className={Styles.content_budget_packet_text}>
                                <p>Com desconto à vista ou parcelado em até 12x</p>
                                <p>Necessária análise do projeto de app*</p>
                            </div>
                            <div className={Styles.content_budget_button}>
                                <Link href='/#contato'><a><button>Orçamento</button></a></Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className={Styles.content_budget_last}>
                            <div className={Styles.content_budget_title}>
                                <p>Sistema personalizado</p>
                            </div>
                            <div className={Styles.content_budget_packet}>
                                <ul>
                                    <li>Aplicações web mais complexas</li>
                                    <li>Desenvolvimento de APIs</li>
                                    <li>Integração de sistemas web e mobile</li>
                                    <li>Sistemas de ERP (Enterprise Resource Planning)</li>
                                    <li>Projetos de software em geral</li>
                                </ul>
                            </div>
                            <div className={Styles.content_budget_button}>
                                <Link href='/#contato'><a><button>Orçamento</button></a></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section >
    )
}