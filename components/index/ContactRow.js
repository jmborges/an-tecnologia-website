import Styles from '../../styles/ContactRow.module.css'
import { useState } from 'react'
import axios from 'axios'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faMobileScreenButton } from '@fortawesome/free-solid-svg-icons'

export default function ContactRow() {
    const [inputs, setInputs] = useState({
        name: '',
        last_name: '',
        email: '',
        subject: '',
        message: '',
    })

    const [form, setForm] = useState('')

    const handleChange = (e) => {
        setInputs((prev) => ({
            ...prev,
            [e.target.id]: e.target.value,
        }))
    }

    const onSubmitForm = async (e) => {
        e.preventDefault()
        if (inputs.name && inputs.email && inputs.message && inputs.subject && inputs.last_name) {
            setForm({ state: 'loading' })
            try {
                const res = await fetch(`api/email`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(inputs),
                })

                const { error } = await res.json()

                if (error) {
                    setForm({
                        state: 'error',
                        message: error,
                    })
                    return
                }

                setForm({
                    state: 'success',
                    message: 'Your message was sent successfully.',
                })
                setInputs({
                    name: '',
                    email: '',
                    message: '',
                })
            } catch (error) {
                setForm({
                    state: 'error',
                    message: 'Something went wrong',
                })
            }
        }
    }
    return (
        <section className={Styles.section_contact} id="contato">
            <div className="container mt-5">
                <h5 className="text-center">Contato</h5>
                <div className="row justify-content-center">
                    <form onSubmit={(e) => onSubmitForm(e)}>
                        <div className="row">
                            <div className="col-xs-6 col-md-6 col-sm-6">
                                <div className="form-group">
                                    <input id="name" name="name" className="form-control" type="text" autoComplete="Nome" placeholder='Nome' required value={inputs.name} onChange={handleChange} />
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6">
                                <div className="form-group">
                                    <input id="last_name" name="last_name" className="form-control" type="text" autoComplete="Sobrenome" placeholder='Sobrenome' required value={inputs.last_name} onChange={handleChange} />
                                </div>
                            </div>
                            <div className="col-xs-12 col-md-12 col-sm-12">
                                <div className="form-group">
                                    <input id="email" name="email" className="form-control" type="text" autoComplete="email" placeholder='E-mail' required value={inputs.email} onChange={handleChange} />
                                </div>
                            </div>
                            <div className="col-xs-12 col-md-12 col-sm-12">
                                <div className="form-group">
                                    <input id="subject" name="subject" className="form-control" type="text" autoComplete="subject" placeholder='Assunto' value={inputs.subject} onChange={handleChange} />
                                </div>
                            </div>
                            <div className="col-xs-12 col-md-12 col-sm-12">
                                <div className="form-group">
                                    <textarea id="message" name="message" className="form-control" cols="30" rows="3" placeholder='Mensagem' value={inputs.message} onChange={handleChange} />
                                </div>
                            </div>

                            <div className="col-xs-5 col-md-12 col-sm-5 text-center  mt-4 mb-3">
                                <button type='submit' className={Styles.button_contact}>Enviar</button>
                            </div>
                            {form.state === 'loading' ? (
                                <>
                                    <div className='col-xs-12 col-md-12 col-sm-12'><div className='d-flex text-center justify-content-center align-center'><div className={Styles.loader}></div></div></div>

                                </>
                            ) : form.state === 'error' ? (
                                <div className='col-xs-12 col-md-12 col-sm-12 alert alert-danger'>{form.message}</div>
                            ) : (
                                form.state === 'success' && <div className='col-xs-12 col-md-12 col-sm-12'><div className='text-center'><svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" color='#4CAF50' fill="currentColor" className="bi bi-check-circle" viewBox="0 0 16 16">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                    <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z" />
                                </svg></div></div>
                            )}
                        </div>
                    </form>
                </div>
            </div>
            <div className='container'>
                <div className='row justify-content-center mt-5'>
                    <div className='col-md-8 col-sm-12'>
                        <div className={Styles.container_contact}>
                            <span className={Styles.container_contact_title + ' text-center'}>Caso preferir, pode falar conosco por meio desses pontos de contato:</span>
                            <div className='row'>
                                <div className='col-xs-12 col-sm-6'>
                                    <div className={Styles.content_contact_icon}>
                                        <FontAwesomeIcon icon={faEnvelope} />
                                    </div>
                                    <div className={Styles.content_contact_text}>
                                        <Link href='mailto:contato@an.tec.br'><a>contato@an.tec.br</a></Link>
                                    </div>
                                    <div className={Styles.content_contact_type}>
                                        <span>E-Mail</span>
                                    </div>
                                </div>
                                <div className='col-xs-12 col-sm-6'>
                                    <div className={Styles.content_contact_icon}>
                                        <FontAwesomeIcon icon={faMobileScreenButton} />
                                    </div>
                                    <div className={Styles.content_contact_text}>
                                        <Link href='https://api.whatsapp.com/send?phone=5554999424184'><a target="_blank"><span>(54) 9 9942 4184</span></a></Link>
                                    </div>
                                    <div className={Styles.content_contact_type}>
                                        <span>Telefone/WhatsApp</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}