import Styles from "../../styles/Portifolio.module.css"
import Image from "next/image";
import Link from "next/link";

export default function PortifolioRow() {
    return (
        <section className={Styles.container_portifolio} id='projetos'>
            <div className="container">
                <div className="row justify-content-center">
                    <h3 className={Styles.title_portifolio}>Nossos Projetos</h3>
                    <div className="col-md-6">
                        <div className={Styles.container_image_portifolio}>
                            <Link href='https://www.plantiodireto.com.br/'>
                                <a className={Styles.container_image_portifolio_inline} target="_blank"><Image src='/images/portifolio_1.png' width='700' height='400' alt='Revista Plantio Direto' title="Revista Plantio Direto" /></a>
                            </Link>
                        </div>
                        <div className={Styles.container_image_portifolio_client}>
                            <Link href='https://www.plantiodireto.com.br/'>
                                <a  target="_blank"><span>Revista Plantio Direto</span></a>
                            </Link>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className={Styles.container_image_portifolio}>
                            <Link href='https://aliado.app.br/'>
                                <a className={Styles.container_image_portifolio_inline} target="_blank"><Image src='/images/portifolio_2.png' width='700' height='400' alt='Aliado - Sistema de Gestão Agrícola' title="Aliado - Sistema de Gestão Agrícola" /></a>
                            </Link>
                        </div>
                        <div className={Styles.container_image_portifolio_client}>
                            <Link href='https://aliado.app.br/'>
                                <a className={Styles.container_image_portifolio_client} target="_blank"><span>Aliado - Sistema de Gestão Agrícola</span></a>
                            </Link>
                        </div>
                    </div>
                    <div className="col-md-6 mt-lg-5">
                        <div className={Styles.container_image_portifolio}>
                            <Link href='https://www.agrocomm.agr.br/'>
                                <a className={Styles.container_image_portifolio_inline} target="_blank"><Image src='/images/portifolio_3.png' priority width='700' height='400' alt='Agência Agrocomm' title="Agência Agrocomm" /></a>
                            </Link>
                        </div>
                        <div className={Styles.container_image_portifolio_client}>
                            <Link href='https://www.agrocomm.agr.br/'>
                                <a className={Styles.container_image_portifolio_client} target="_blank"><span>Agência Agrocomm</span></a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}