import Image from "next/image"
import Styles from "../../styles/ServicesRow.module.css"
export default function ServicesRow() {
    return (
        <>
            <section className={Styles.bg_nossos_projetos} id="servicos">
                <div className="container">
                    <div className="row justify-content-center mt-5">
                        <div className='col-xs-12 col-sm-12 col-md-12'>
                            <h2 className={Styles.title_services}>Serviços oferecidos</h2>
                        </div>
                        <div className="col-lg-3 col-xs-12 col-sm-12">
                            <div className={Styles.services_content}>
                                <div className="service-icon">
                                    <Image src="/images/web-programming.png" className="img-fluid" width="70" height="70" alt="Site responsivo" />
                                </div>
                                <p className={Styles.services_title}>
                                    Desenvolvimento de sites
                                </p>
                                <div className={Styles.services_text}>
                                    <p>Desenvolvemos sites de todos os tipos: Do simples ao complexo - landing pages, sites institucionais, aplicações web - com uso de tecnologias modernas como React e Vue.JS</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-xs-12 col-sm-12">
                            <div className={Styles.services_content}>
                                <div className="service-icon">
                                    <Image src="/images/online-shopping.png" className="img-fluid" width="70" height="70" alt="Desenvolvimento PHP" />
                                </div>
                                <p className={Styles.services_title}>
                                    Desenvolvimento de loja virtual (E-Commerce)
                                </p>
                                <div className={Styles.services_text}>
                                    <p>Precisa vender online? Podemos desenvolver uma solução de e-commerce personalizada para o seu negócio.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-xs-12 col-sm-12">
                            <div className={Styles.services_content}>
                                <div className="service-icon">
                                    <Image src="/images/responsivo.png" className="img-fluid" width="70" height="70" alt="Landing Page" />
                                </div>
                                <p className={Styles.services_title}>
                                    Desenvolvimento de aplicativos mobile
                                </p>
                                <div className={Styles.services_text}>
                                    <p>Desenvolvemos seu app para aparelhos mobile utilizando tecnologias rápidas, seguras e universais como React Native e Flutter.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-xs-12 col-sm-12">
                            <div className={Styles.services_content}>
                                <div className="service-icon">
                                    <Image src="/images/landing-page.png" className="img-fluid" width="70" height="70" alt="E-Commerce" />
                                </div>
                                <p className={Styles.services_title}>
                                    Projetos personalizados
                                </p>
                                <div className={Styles.services_text}>
                                    <p>Se você precisa de um sistema feito sob medida para resolver os seus problemas, usar melhor seu tempo e otimizar processos, entre em contato conosco para que possamos ajudar.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}