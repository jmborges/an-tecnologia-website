import Styles from "../../styles/Clients.module.css"
import Image from "next/image";

export default function ClientsRow() {
    return (
        <section className={Styles.container_clients} id="clientes">
            <div className='container'>
                <div className='row justify-content-center'>
                    <div className='col-md-12'>
                        <h4 className={Styles.title_clients}>Clientes!</h4>
                        <div className={Styles.slider}>
                            <div className={Styles.slide_track}>
                                <div className={Styles.slide}>
                                    <Image src="/images/client_1.png" height="120" width="250" alt="ok" />
                                </div>
                                <div className={Styles.slide}>
                                    <Image src="/images/client_2.png" height="120" width="250" alt="ok" />
                                </div>
                                <div className={Styles.slide}>
                                    <Image src="/images/client_1.png" height="120" width="250" alt="ok" />
                                </div>
                                <div className={Styles.slide}>
                                    <Image src="/images/client_2.png" height="120" width="250" alt="ok" />
                                </div>
                                <div className={Styles.slide}>
                                    <Image src="/images/client_1.png" height="120" width="250" alt="ok" />
                                </div>
                                <div className={Styles.slide}>
                                    <Image src="/images/client_2.png" height="120" width="250" alt="ok" />
                                </div>
                                <div className={Styles.slide}>
                                    <Image src="/images/client_1.png" height="120" width="250" alt="ok" />
                                </div>
                                <div className={Styles.slide}>
                                    <Image src="/images/client_2.png" height="120" width="250" alt="ok" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    );
}