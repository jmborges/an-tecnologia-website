import Head from 'next/head'
import Script from 'next/script'
import Link from 'next/link'
import Header from './Header'
import Footer from './Footer'
import 'jquery/dist/jquery.min.js'
import 'bootstrap/dist/css/bootstrap.css'


export default function Layout({ children }) {
    return (
        <>
            <Head>
                <title>AN Tecnologia</title>
                <meta name="description" content="Empresa focada no desenvolvimento de sites, aplicativos e projetos personalizados de software com excelente custo/benefício" />
                <meta charSet="UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="icon" href="/favicon.ico" />
                <meta property="og:title" content="AN Tecnologia" />
                <meta property="og:type" content="website" />
                <meta property="og:description" content="Empresa focada no desenvolvimento de sites, aplicativos e projetos personalizados de software com excelente custo/benefício" />
                <meta property="og:image" content="https://an.tec.br/_next/image?url=%2Fimages%2Flogo.jpg&w=640&q=75" />
                <meta property="og:url" content="https://an.tec.br" />
                <meta property="og:site_name" content="AN Tecnologia" />
                <meta name="google-site-verification" content="OoDsYHjHR370hm7SocicJkv3XhBWhqBGthOganXfsZ8" />
                <noscript dangerouslySetInnerHTML={{ __html: `<img height="1" width="1" style='display: none' src='https://www.facebook.com/tr?id=651188409772729&ev=PageView&noscript=1'/>`, }}></noscript>
                <noscript dangerouslySetInnerHTML={{ __html: `<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=4486321&fmt=gif" />`, }}></noscript>
            </Head>
            <Script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous" />
            <Script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous" />
            <Header />
            <main>{children}</main>
            <Footer />
            <Script strategy="afterInteractive" id='facebook-pixel'
                dangerouslySetInnerHTML={{
                    __html: `
                        !function(f,b,e,v,n,t,s)
                        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                        n.queue=[];t=b.createElement(e);t.async=!0;
                        t.src=v;s=b.getElementsByTagName(e)[0];
                        s.parentNode.insertBefore(t,s)}(window, document,'script',
                        'https://connect.facebook.net/en_US/fbevents.js');
                        fbq('init', '651188409772729');
                        fbq('track', 'PageView');
                    `,
                }}
            />
            <Script strategy="afterInteractive" id='linkedin-pixel'
                dangerouslySetInnerHTML={{
                    __html: `
                        _linkedin_partner_id = "4486321";
                        window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
                        window._linkedin_data_partner_ids.push(_linkedin_partner_id);
                    `,
                }}
            />
            <Script strategy="afterInteractive" id='linkedin-pixel_2'
                dangerouslySetInnerHTML={{
                    __html: `
                        (function(l) {
                            if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])};
                            window.lintrk.q=[]}
                            var s = document.getElementsByTagName("script")[0];
                            var b = document.createElement("script");
                            b.type = "text/javascript";b.async = true;
                            b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
                            s.parentNode.insertBefore(b, s);})(window.lintrk);
                    `,
                }}
            />
            <Script strategy="afterInteractive" src={`https://www.googletagmanager.com/gtag/js?id=G-Q7VQGB9HXV%22%3E/`} />
            <Script id='script-google' strategy="afterInteractive"
                dangerouslySetInnerHTML={{
                    __html: `window.dataLayer = window.dataLayer || [];
                                function gtag(){dataLayer.push(arguments);}
                                gtag('js', new Date());
                                gtag('config', 'G-Q7VQGB9HXV');`
                }}
            />
            {/* <Script strategy="afterInteractive" id='tawk'
                dangerouslySetInnerHTML={{
                    __html: `
                    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                    (function(){
                    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                    s1.async=true;
                    s1.src='https://embed.tawk.to/62a8d4817b967b1179948ab7/1g5hns74k';
                    s1.charset='UTF-8';
                    s1.setAttribute('crossorigin','*');
                    s0.parentNode.insertBefore(s1,s0);
                    })();
                    `,
                }}
            /> */}
        </>
    )
}