import styles from '../styles/Footer.module.css'
import Image from 'next/image'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook } from '@fortawesome/free-brands-svg-icons'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { faYoutube } from '@fortawesome/free-brands-svg-icons'


export default function Footer() {
  return (
    <footer className={styles.footer}>
      <div className='container'>
        <div className='row'>
          <div className='col-xs-12 col-sm-3 pb-3'>
            <div className={styles.footer_title}>Menu</div>
            <ul>
              <li>
                <Link href="/#servicos" scroll={true}><a >Serviços oferecidos</a></Link>
              </li>
              <li>
                <Link href="/#projetos" scroll={true}><a >Nossos projetos</a></Link>
              </li>
              {/* <li>
                <Link href="/#clientes"><a >Clientes</a></Link>
              </li> */}
              <li>
                <Link href="/#orcamento" scroll={true}><a>Orçamento</a></Link>
              </li>
              <li>
                <Link href="/#contato" scroll={true}><a>Contato</a></Link>
              </li>
              <li>
                <Link href="/blog"><a>Blog</a></Link>
              </li>

            </ul>
          </div>
          <div className='col-xs-12 col-sm-3 pb-3'>
            <div className={styles.footer_title}>Privacidade</div>
            <ul>
              <li>
                <Link href="/privacyPolicy"><a>Política de Privacidade</a></Link>
              </li>
            </ul>
          </div>
          <div className='col-sm-12 col-xs-12 col-md-2'>
            <Link href="/"><a className="navbar-brand text-white"><Image src='/images/logo.png' width={410} height={100} title="AN Tecnologia" alt="AN Tecnologia"></Image></a></Link>
            <div className={styles.footer_social_media}>
              <Link href="https://www.facebook.com/an.tecnologia.ltda"><a target="_blank"><FontAwesomeIcon icon={faFacebook} size='2x'/></a></Link>
              <Link href="https://www.instagram.com/an.tecnologia.ltda"><a target="_blank"><FontAwesomeIcon icon={faInstagram}/></a></Link>
              <Link href="https://www.linkedin.com/company/an-tecnologia"><a target="_blank"><FontAwesomeIcon icon={faLinkedin}/></a></Link>
              <Link href="https://www.youtube.com/channel/UC9UefNrUYpDG-7eiMpTg_Sg"><a target="_blank"><FontAwesomeIcon icon={faYoutube}/></a></Link>
            </div>
          </div>
        </div>
        <div className='text-center pb-2'>
          <strong>© 2022 - Todos os direitos reservados - AN Tecnologia</strong>
        </div>
      </div>
    </footer>
  )
}