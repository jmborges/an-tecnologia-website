import Link from "next/link"
import Image from "next/image"
import Styles from "../styles/Navbar.module.css"
export default function Navbar() {
    return (
        <div className="container">
            <nav className="navbar navbar-expand-lg navbar-light">
                <div className="container-fluid">
                <div className={Styles.img_logo} >
                <Link href="/"><a className="navbar-brand text-white"><Image src='/images/logo.png' width={500} height={100} title="AN Tecnologia" alt="AN Tecnologia"></Image></a></Link>
                </div>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className={Styles.nav_toggler_custom_icon + ' navbar-toggler-icon'}></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav m-auto">
                            <li className="nav-item">
                                <Link href="/#servicos" scroll={true}><a className={Styles.nav_link} >Serviços oferecidos</a></Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/#projetos" scroll={true}><a className={Styles.nav_link} >Nossos projetos</a></Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/#orcamento" scroll={true}><a className={Styles.nav_link}>Orçamento</a></Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/#contato" scroll={true}><a className={Styles.nav_link}>Contato</a></Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/blog"><a className={Styles.nav_link}>Blog</a></Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}