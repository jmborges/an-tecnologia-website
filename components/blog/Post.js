import Link from "next/link";
import Image from "next/image";
import Styles from "/./styles/Blog.module.css"
import CoverImage from "./CoverImage";
import DateFormatter from "./DateFormatter";
export default function Post({
  title,
  excerpt,
  slug,
  coverImage,
  date
}) {
  return (
    <div className="col-md-6 col-xs-12 col-sm-12 mb-5">
      <article className={Styles.container_post}>
        <CoverImage
          title={title}
          src={coverImage}
          slug={slug}
          height={620}
          width={1240} />
        <div className={Styles.content_post}>
          <h3 className={Styles.title_post}>
            <Link href={`/posts/${slug}/`}>
              <a>{title}</a>
            </Link>
          </h3>
          <div  className={Styles.date}>
            <DateFormatter dateString={date} />
          </div>
          <div className={Styles.content_excerpt}>
            <p>{excerpt}</p>
            {/* <Avatar name={author.name} picture={author.picture} /> */}
          </div>
        </div>
      </article>
    </div>
  )
}