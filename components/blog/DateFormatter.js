import { parseISO, format } from 'date-fns'

export default function DateFormatter({ dateString }) {
  const date = parseISO(dateString)
  return <time dateTime={dateString}>{format(date, 'dd/MM/yyyy', {
    timeZone: 'America/Sao_Paulo',
  }) }</time>
}