import Link from 'next/link'
import Image from 'next/image'
export default function CoverImage({ title, src, slug, height, width }) {
    const image = (
        <Image
          src={src}
          alt={title}
          className={''}
          layout="responsive"
          width={width}
          height={height}
          priority
        />
      )
      return (
        <div className="">
          {slug ? (
            <Link href={`/posts/${slug}/`}>
              <a aria-label={title}>{image}</a>
            </Link>
          ) : (
            image
          )}
        </div>
      )
}