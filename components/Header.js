import Navbar from "./Navbar"
import Styles from "../styles/Header.module.css"
export default function Headers() {
    return (
        <header className={Styles.header}>
            <Navbar />
        </header>
    )
}